from django.shortcuts import redirect, render
from lists.models import Survey
from datetime import datetime

def home_page(request):
    if request.method == 'POST'and request.POST.get(
                                    'add_survey', '') == 'Submit':
        question_text = request.POST['question']
        choice1 = request.POST['choice_1']
        choice2 = request.POST['choice_2']
        choice3 = request.POST['choice_3']
        choice4 = request.POST['choice_4']
        Survey.objects.create(question = question_text, choice1=choice1 ,
                             choice2 = choice2, choice3 = choice3, 
                             choice4 = choice4,time = datetime.now())
        return redirect('/')

    if request.method == 'POST'and request.POST.get(
                                    'nextpage', '') == 'Answer':
        return redirect('/answer/')

    return render(request, 'home.html')

def answer_page(request):

    showquestion = Survey.objects.all()
    if request.method == 'POST'and request.POST.get(
                                    'homepage', '') == 'Add_Question':
        return redirect('/')

    if request.method == 'POST'and request.POST.get(
                                    'add_answer', '') == 'Submit':
        for i in showquestion:
            result = request.POST[i.question]
            if result == i.choice1:
                i.answer1 = int(i.answer1)+1
            elif result == i.choice2:
                i.answer2 = int(i.answer2)+1
            elif result == i.choice3:
                i.answer3 = int(i.answer3)+1
            else:
                i.answer4 = int(i.answer4)+1
            i.save()

        return redirect('/result/')

    return render(request, 'answer.html',{'showquestion': showquestion})
       

def result_page(request):

    if request.method == 'POST'and request.POST.get(
                                    'back', '') == 'Back':
        return redirect('/answer/')

    showresult = Survey.objects.all()

    return render(request, 'result.html', {'showresult': showresult})
