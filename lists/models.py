from django.db import models
from datetime import datetime

class Survey(models.Model):
    question = models.TextField(default='')
    choice1 = models.TextField(default='')
    choice2 = models.TextField(default='')
    choice3 = models.TextField(default='')
    choice4 = models.TextField(default='')
    answer1 = models.IntegerField(default=0)
    answer2 = models.IntegerField(default=0)
    answer3 = models.IntegerField(default=0)
    answer4 = models.IntegerField(default=0)
    time = models.DateTimeField(default=datetime.now, blank=True)
