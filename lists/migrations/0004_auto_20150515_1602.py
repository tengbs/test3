# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0003_auto_20150514_0756'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey',
            name='answer1',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey',
            name='answer2',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey',
            name='answer3',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey',
            name='answer4',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
