from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()


    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_show_question')
        rows = table.find_elements_by_tag_name('td')
        self.assertIn(row_text, [row.text for row in rows])


    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)

        self.assertIn('Survey', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Create Your Question', header_text)

        questionbox = self.browser.find_element_by_id('id_question')
        self.assertEqual(
                questionbox.get_attribute('placeholder'),
                'Key a Question'
        )

        questionbox.send_keys('What is your favourite color')

        choicebox1 = self.browser.find_element_by_id('id_choice_1')
        choicebox1.send_keys('red')
        choicebox2 = self.browser.find_element_by_id('id_choice_2')
        choicebox2.send_keys('green')
        choicebox3 = self.browser.find_element_by_id('id_choice_3')
        choicebox3.send_keys('blue')
        choicebox4 = self.browser.find_element_by_id('id_choice_4')
        choicebox4.send_keys('yellow')
        button = self.browser.find_element_by_id('id_submit').click()

        questionbox = self.browser.find_element_by_id('id_question')
        questionbox.send_keys('What is your favourite animal')
        choicebox1 = self.browser.find_element_by_id('id_choice_1')
        choicebox1.send_keys('lion')
        choicebox2 = self.browser.find_element_by_id('id_choice_2')
        choicebox2.send_keys('cat')
        choicebox3 = self.browser.find_element_by_id('id_choice_3')
        choicebox3.send_keys('bird')
        choicebox4 = self.browser.find_element_by_id('id_choice_4')
        choicebox4.send_keys('dog')

        button = self.browser.find_element_by_id('id_submit').click()
        button = self.browser.find_element_by_id('id_nextpage').click()

        self.check_for_row_in_list_table('Question')
        self.check_for_row_in_list_table('choice 1')
        self.check_for_row_in_list_table('choice 2')
        self.check_for_row_in_list_table('choice 3')
        self.check_for_row_in_list_table('choice 4')
        self.check_for_row_in_list_table('What is your favourite color')
        self.check_for_row_in_list_table('red')
        self.check_for_row_in_list_table('green')
        self.check_for_row_in_list_table('blue')
        self.check_for_row_in_list_table('yellow')

        button = self.browser.find_element_by_id('id_submit').click()

        self.check_for_row_in_list_table('What is your favourite color')
        self.check_for_row_in_list_table('red ( 1 )')
        self.check_for_row_in_list_table('green ( 0 )')
        self.check_for_row_in_list_table('blue ( 0 )')
        self.check_for_row_in_list_table('yellow ( 0 )')
        button = self.browser.find_element_by_id('id_back').click()
        button = self.browser.find_element_by_id('id_submit').click()
        self.check_for_row_in_list_table('What is your favourite color')
        self.check_for_row_in_list_table('red ( 2 )')
        self.check_for_row_in_list_table('green ( 0 )')
        self.check_for_row_in_list_table('blue ( 0 )')
        self.check_for_row_in_list_table('yellow ( 0 )')

        self.fail('Finish the test!')


